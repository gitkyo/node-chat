import express from 'express'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import http from 'http'
import path from 'path'
import { Server } from "socket.io";
import Filter from 'bad-words'
import { generateMessage, generateLocationMessage } from './utils/message.js' 
import { addUser, removeUser, getUser, getUsersInRoom } from './utils/users.js'


//set variables for public directory
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const publishDirectoryPath = path.join(__dirname, '../public')
const port = process.env.PORT || 3000 

//create server
const app = express()
const server = http.createServer(app)
const io = new Server(server); 

//set public directory
app.use(express.static(publishDirectoryPath))


// recap
// server (emit) -> client (receive) - countUpdated -- 
// client (emit) -> server (receive) - increment --


io.on('connection', (socket) => { 
    console.log('New webSocket connected');

    //emit to the client when connexion is established
    // socket.emit('message', generateMessage('Welcome!')) //emit to the client who just connected      
    // socket.broadcast.emit('message', 'A new user has joined!') //emit to all client except the one who just connected

    socket.on('join', (options, callback ) => {

        const { error, user } = addUser({ id: socket.id, ...options })

        if(error){
            return callback(error)
        }

        socket.join(user.room)

        socket.emit('message', generateMessage('Admin', 'Welcome!')) //emit to the client who just connected
        socket.broadcast.to(user.room).emit('message', generateMessage('Admin', `${user.username} has joined!`)) //emit to all client except the one who just connected

        callback()
        // socket.emit, io.emit, socket.broadcast.emit
        // io.to.emit, socket.broadcast.to.emit
    })


    //receive event from client and emit to all client
    socket.on('sendMessage', (message, callback) => {

        const user = getUser(socket.id)        
        
        //filter profanity to exclude bad words
        const filter = new Filter()

        if(filter.isProfane(message)){
            return callback('Profanity is not allowed!')
        }
       

        //envoi du message aux client du salon
        io.to(user.room).emit('message', generateMessage(user.username, message))
        
        //callback pour gérer l'erreur
        callback()
        
    })

    //receive location from client and emit to all client
    socket.on('sendLocation', (coords, callback) => {
        const user = getUser(socket.id)
        io.to(user.room).emit('locationMessage', generateLocationMessage(user.username, `https://google.com/maps?q=${coords.latitude},${coords.longitude}`))
        callback()
    })

    //disconnect event
    socket.on('disconnect', () => {

        const user = removeUser(socket.id)

        if(user){
            io.to(user.room).emit('message', generateMessage(user.username, `${user.username} has left!`))
        }

        // i    o.emit('message', generateMessage('A user has left!'))
    })


 })

//start server
server.listen(port, () =>{
    console.log('server listening on port: ' + port)
})

